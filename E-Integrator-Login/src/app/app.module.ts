﻿import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
// used to create fake backend
import {BasicAuthInterceptor, ErrorInterceptor, fakeBackendProvider} from './_helpers';

import {AppComponent} from './app.component';
import {appRoutingModule} from './app.routing';
import {HomeComponent} from './home';
import {LoginComponent} from './login';
import {scratchpad} from '@app/scratchpad/scratchpad.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {ModalModule} from '@app/_modal';

@NgModule({
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    appRoutingModule,
    DragDropModule,
    FormsModule,
    ModalModule
  ],
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    scratchpad
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: BasicAuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},

    // provider used to create fake backend
    fakeBackendProvider,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
