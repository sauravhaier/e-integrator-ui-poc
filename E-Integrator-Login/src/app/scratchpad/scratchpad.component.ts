/**
 * @title Drag&Drop disabled sorting
 */

import {Component, OnInit} from '@angular/core';
import {CdkDragDrop, copyArrayItem, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {ModalService} from '@app/_modal';

/**
 * @title Drag&Drop connected sorting group
 */
@Component({
  selector: 'ahoy-scratchpad.component',
  templateUrl: 'scratchpad.component.html',
  styleUrls: ['scratchpad.component.css'],
})
export class scratchpad implements OnInit {
  constructor(private modalService: ModalService) {
  }

  bodyText: string;

  todo = [
    'AMP',
    'ABC',
    'XYZ'
  ];

  done = [
    'NETSUITE',
    'ACCUMATICA',
    'SAP HANNA'
  ];

  previousData = [];

  dropHere = [];
  erp = [];
  loading = false;

  drop(event: CdkDragDrop<string[]>) {
    if (event.container.id == 'console') {
      this.loading = true;
      this.modalService.index = event.currentIndex;
      this.modalService.open('custom-modal-1');
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      } else {
        copyArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      }
    }
  }

  dropErp(event: CdkDragDrop<string[]>) {
    if (event.container.id == 'console2') {
      this.loading = true;
      this.modalService.index = event.currentIndex;
      this.modalService.open('custom-modal-1');
      if (event.previousContainer === event.container) {
        moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      } else {
        copyArrayItem(event.previousContainer.data,
          event.container.data,
          event.previousIndex,
          event.currentIndex);
      }
    }
  }

  close(item) {
    const index = this.dropHere.indexOf(item);
    if (index > -1) {
      this.dropHere.splice(index, 1);
    }

  }
  closeErp(item) {
    const index = this.erp.indexOf(item);
    if (index > -1) {
      this.erp.splice(index, 1);
    }

  }

  openModal(id: string) {
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
    if (this.modalService.index > -1) {
      this.dropHere.splice(this.modalService.index, 1);
    }
  }

  submit() {
    this.modalService.close('custom-modal-1');
  }

  ngOnInit(): void {
    this.loading = true;
  }
}
